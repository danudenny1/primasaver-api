FROM java:8
EXPOSE 8333
ADD primasaver-api/target/primasaver-api-0.0.1-SNAPSHOT.jar primasaver-api.jar
ENTRYPOINT ["java", "-jar", "/primasaver-api.jar"]