package com.primasaver.api.repository;

import com.primasaver.api.domain.Panel;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PanelRepository extends
    PagingAndSortingRepository<Panel, Long>,
    QuerydslPredicateExecutor<Panel> {
        List<Panel> findByPanelNameLikeIgnoringCase(String panelName);
    }
