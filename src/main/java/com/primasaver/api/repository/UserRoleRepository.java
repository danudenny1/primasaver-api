package com.primasaver.api.repository;

import com.primasaver.api.domain.UserRole;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRoleRepository extends
        PagingAndSortingRepository<UserRole, Long>,
        QuerydslPredicateExecutor<UserRole> {

    Optional<UserRole> findByUserId (Long aLong);

    List<UserRole> findByRoleId (Long roleId);
}
