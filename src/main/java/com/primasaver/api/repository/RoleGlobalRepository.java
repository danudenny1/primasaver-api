package com.primasaver.api.repository;

import com.primasaver.api.domain.Role;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleGlobalRepository extends
        PagingAndSortingRepository<Role, Long>,
        QuerydslPredicateExecutor<Role> {

    Optional<Role> findByGroupDesc(String groupDesc);
}
