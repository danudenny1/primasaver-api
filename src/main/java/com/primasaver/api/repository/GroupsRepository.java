package com.primasaver.api.repository;

import com.primasaver.api.domain.Groups;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GroupsRepository extends
        PagingAndSortingRepository<Groups, Long>,
        QuerydslPredicateExecutor<Groups> {

    Optional<Groups> findByName(String name);
}
