package com.primasaver.api.repository;

import com.primasaver.api.dto.UserDTO;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserCustomRepository extends
        PagingAndSortingRepository<UserDTO, Long>,
        QuerydslPredicateExecutor<UserDTO> {
    List<UserDTO> findByUsernameLikeIgnoringCase(String username);
    UserDTO findByUsername(String username);
}
