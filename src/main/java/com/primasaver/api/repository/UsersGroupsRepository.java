package com.primasaver.api.repository;

import com.primasaver.api.domain.UsersGroups;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsersGroupsRepository extends
        PagingAndSortingRepository<UsersGroups, Long>,
        QuerydslPredicateExecutor<UsersGroups> {

    Optional<UsersGroups> findByUserId(Long aLong);

    List<UsersGroups> findByGroupId(Long groupId);
}

