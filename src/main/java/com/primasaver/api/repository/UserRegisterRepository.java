package com.primasaver.api.repository;

import com.primasaver.api.domain.UserRegister;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRegisterRepository extends
        PagingAndSortingRepository<UserRegister, Long>,
        QuerydslPredicateExecutor<UserRegister> {

    Optional<UserRegister> findByUsername(String username);
}
