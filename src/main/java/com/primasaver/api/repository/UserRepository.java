package com.primasaver.api.repository;

import com.primasaver.api.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface UserRepository extends
        PagingAndSortingRepository<User, Long>,
        QuerydslPredicateExecutor<User> {
    List<User> findByUsernameLikeIgnoringCase(String username);
    User findByUsername(String username);
}
