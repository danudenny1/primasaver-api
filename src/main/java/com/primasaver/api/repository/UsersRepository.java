package com.primasaver.api.repository;

import com.primasaver.api.domain.Users;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepository extends
        PagingAndSortingRepository<Users, Long>,
        QuerydslPredicateExecutor<Users> {
    List<Users> findByUsernameLikeIgnoringCase(String username);
    Users findByUsername(String username);
}

