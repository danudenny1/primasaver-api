package com.primasaver.api.repository;

import com.primasaver.api.domain.ExpiredToken;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface ExpiredTokenRepository extends
        PagingAndSortingRepository<ExpiredToken, Long>,
        QuerydslPredicateExecutor<ExpiredTokenRepository> {
            Optional<ExpiredToken> findByToken(String token);

        }
