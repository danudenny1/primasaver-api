package com.primasaver.api.repository;

import com.primasaver.api.domain.Device;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceRepository extends
        PagingAndSortingRepository<Device, Long>,
        QuerydslPredicateExecutor<Device> {
    List<Device> findByDeviceIdentityIdLikeIgnoringCase(String deviceIdentityId);


    List<Device> findByClientId(Long idClient);
}
