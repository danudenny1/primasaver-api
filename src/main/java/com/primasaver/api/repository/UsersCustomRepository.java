package com.primasaver.api.repository;

import com.primasaver.api.dto.UsersDTO;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersCustomRepository extends
        PagingAndSortingRepository<UsersDTO, Long>,
        QuerydslPredicateExecutor<UsersDTO> {
    List<UsersDTO> findByUsernameLikeIgnoringCase(String username);
    UsersDTO findByUsername(String username);
}
