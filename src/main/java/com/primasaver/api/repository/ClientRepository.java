package com.primasaver.api.repository;

import com.primasaver.api.domain.Client;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends
        PagingAndSortingRepository<Client, Long>,
        QuerydslPredicateExecutor<Client> {
    List<Client> findByCompanyLikeIgnoringCase(String company);
}
