package com.primasaver.api.repository;


import com.primasaver.api.domain.Zona;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ZonaRepository extends
        PagingAndSortingRepository<Zona, Long>,
        QuerydslPredicateExecutor<Zona> {
    List<Zona> findByZonaNameLikeIgnoringCase(String zonaName);
}
