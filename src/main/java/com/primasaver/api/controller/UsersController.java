package com.primasaver.api.controller;

import com.primasaver.api.dao.UsersGroupsDAO;
import com.primasaver.api.domain.*;
import com.primasaver.api.dto.QUsersDTO;
import com.primasaver.api.dto.UsersDTO;
import com.primasaver.api.dto.UsersGroupsDTO;
import com.primasaver.api.repository.*;
import com.primasaver.api.security.JwtTokenUtil;
import com.primasaver.api.service.ResourceNotFoundException;
import com.primasaver.api.service.ResponseDuplicateKeyException;
import com.querydsl.core.BooleanBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/user")
@Api(tags = "Users")
public class UsersController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsersController.class);

    @Autowired
    private UsersRepository userRepo;

    @Autowired
    private UsersCustomRepository userCustomRepo;

    @Autowired
    private UserRegisterRepository userRegisterRepo;

    @Autowired
    private GroupsRepository groupsRepo;

    @Autowired
    private UsersGroupsRepository usersGroupsRepo;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    UsersGroupsDAO usersGroupsDAO;

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    @Qualifier("jwtUserDetailsService")
    private UserDetailsService userDetailsService;

    // ---------- Encrypt Pass  word ----------
    public UsersController(UsersRepository userRepo, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepo = userRepo;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    private void setDetails(Users user) {
        Optional<UsersGroups> usersGroups = usersGroupsRepo.findByUserId(user.getId());
        user.setUserLevelText(Authorities.getById(user.getUserLevel()));
        user.setPassdev("");
        if(usersGroups.isPresent()){
            Optional<Groups> groups = groupsRepo.findById(usersGroups.get().getGroupId());
            user.setGroupId(groups.get().getId());
            user.setGroupIdText(groups.get().getDescription());
        }
    }

    private void setDetailUsersGroups(UsersGroupsDTO user) {
        user.setUserLevelText(Authorities.getById(user.getUserLevel()));
    }

    private void saveToUsersGroups(UserRegister user) {
        UsersGroups usersGroups = new UsersGroups();
//        usersGroups.setGroupId(user.getGroupId());
        usersGroups.setUserId(user.getId());
        usersGroupsRepo.save(usersGroups);
    }

    private void setDetails (UsersDTO user) {
        Optional<UsersGroups> usersGroups = usersGroupsRepo.findByUserId(user.getId());
        user.setUserLevelText(Authorities.getById(user.getUserLevel()));
        if(usersGroups.isPresent()){
            Optional<Groups> groups = groupsRepo.findById(usersGroups.get().getGroupId());
            user.setGroupId(groups.get().getId());
            user.setGroupIdText(groups.get().getDescription());
        }
    }

    @GetMapping(path = "")
    @ApiOperation("Get Users")
    public Iterable<UsersDTO> getUsers(
        @RequestHeader("Authorization") String Authorization,
        @RequestParam(name = "username", required = false) String username,
        @RequestParam(name = "firstName", required = false) String firstName,
        @RequestParam(name = "lastName", required = false) String lastName,
        @RequestParam(name = "email", required = false) String email) {
        try {
            QUsersDTO users = QUsersDTO.usersDTO;
            BooleanBuilder builder = new BooleanBuilder();

            if (username != null)
                builder.and(users.username.eq(username));
            if (firstName != null)
                builder.and(users.firstName.eq(firstName));
            if (lastName != null)
                builder.and(users.lastName.eq(lastName));
            if (email != null)
                builder.and(users.email.eq(email));

            Iterable<UsersDTO> all = userCustomRepo.findAll(builder);

            for (UsersDTO value : all) {
                setDetails(value);
            }
            return all;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @GetMapping(path = "/page")
    @ApiOperation("Get Users With Page")
    public Page<Users> getUserWithPages (
        @RequestHeader("Authorization") String Authorization,
        @RequestParam(name = "username", required = false) String username,
        Pageable pageRequest) {
        try {
            QUsers users = QUsers.users;
            BooleanBuilder builder = new BooleanBuilder();

            if (username != null)
                builder.and(users.username.likeIgnoreCase('%'+username +'%'));

            Page<Users> paged = userRepo.findAll(builder, pageRequest);
            for (Users value : paged.getContent()) {
                setDetails(value);
            }

            return paged;
        } catch (Exception e) {
            throw e;
        }
    }

    @GetMapping(path = "/{id}")
    @ApiOperation("Get User By Id")
    public Optional<UsersDTO> getUsersById(
        @RequestHeader("Authorization") String Authorization,
        @PathVariable(value = "id") Long id) {
        try {
            Optional<UsersDTO> resource = userCustomRepo.findById(id);
            setDetails(resource.get());
            return resource;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @GetMapping(path = "/group/{groupId}")
    @ApiOperation("Get User By Group Id")
    public List<UsersGroupsDTO> getUsersByGroupId(
        @RequestHeader("Authorization") String Authorization,
        @PathVariable(value = "groupId") Long groupId) {
        try {
            List<UsersGroupsDTO> resource = usersGroupsDAO.getUsersGroups(groupId);
            for (UsersGroupsDTO value : resource) {
                setDetailUsersGroups(value);
            }
            return resource;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @GetMapping(path = "detail/{id}")
    @ApiOperation("Get User By Id")
    public Optional<Users> getUserDetailById(
            @RequestHeader("Authorization") String Authorization,
            @PathVariable(value = "id") Long id) {
        try {
            Optional<Users> response = userRepo.findById(id);
            setDetails(response.get());
            return  response;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @PostMapping(path = "")
    @ApiOperation("Add User")
    public UserRegister createUser (
        @RequestHeader("Authorization") String Authorization,
        @RequestBody UserRegister request) {
        try {
            String newPassword;

            Optional<UserRegister> dataUser = userRegisterRepo.findByUsername(request.getUsername());
            if (dataUser.isPresent())
                throw new ResponseDuplicateKeyException("User", "Username", request.getUsername());

                newPassword = request.getPassword();

            if(request.getUserLevel()!=null){
                request.setUserLevel(request.getUserLevel());
            }else{
                request.setUserLevel(2);
            }

            if(request.getActive()!=null){
                request.setActive(request.getActive());
            }else{
                request.setActive(0);
            }

            request.setPassdev(newPassword);
            request.setCreatedOn((int) (new Date().getTime()/1000));
            request.setLastLogin((int) (new Date().getTime()/1000));
            request.setPassword(bCryptPasswordEncoder.encode(newPassword));
            UserRegister newUser = userRegisterRepo.save(request);
//            if(newUser.getId()!=null){
//                saveToUsersGroups(request);
//                return newUser;
//            }else{
//                return null;
//            }
            return newUser;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @PutMapping(path = "/{id}")
    @ApiOperation("Update User By Id")
    public UserRegister updateUser(
        @RequestHeader("Authorization") String Authorization,
        @RequestBody UserRegister request,
        @PathVariable Long id) {
        try {
            Optional<Users> resource = userRepo.findById(id);
            if (!resource.isPresent())
                throw new ResourceNotFoundException("User", "Id", id);

            request.setId(id);
            if(request.getPassword()!=null){
                request.setPassdev(request.getPassword());
                request.setPassword(bCryptPasswordEncoder.encode(request.getPassdev()));
            }else{
                request.setPassdev(resource.get().getPassdev());
                request.setPassword(resource.get().getPassword());
            }

            if(request.getUsername()==null)
                request.setUsername(resource.get().getUsername());

            if(request.getEmail()==null)
                request.setEmail(resource.get().getEmail());

            if(request.getUserLevel()==null)
                request.setUserLevel(resource.get().getUserLevel());

            if(request.getActive()==null)
                request.setActive(resource.get().getActive());

            request.setCreatedOn((int) (new Date().getTime()/1000));
            request.setLastLogin((int) (new Date().getTime()/1000));
        	return userRegisterRepo.save(request);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @PutMapping(path = "/account_info/{id}")
    @ApiOperation("Update Account User By Id")
    public UserRegister updateInfoAccountUser(
        @RequestHeader("Authorization") String Authorization,
        @RequestBody UserRegister request,
        @PathVariable Long id) {
        try {
            Optional<Users> resource = userRepo.findById(id);
            if (!resource.isPresent())
                throw new ResourceNotFoundException("User", "Id", id);

            request.setId(id);
            if(request.getPassword()!=null){
                request.setPassdev(request.getPassword());
                request.setPassword(bCryptPasswordEncoder.encode(request.getPassdev()));
            }else{
                request.setPassdev(resource.get().getPassdev());
                request.setPassword(resource.get().getPassword());
            }

            if(request.getUsername()==null)
                request.setUsername(resource.get().getUsername());

            if(request.getEmail()==null)
                request.setEmail(resource.get().getEmail());

            if(request.getUserLevel()==null)
                request.setUserLevel(resource.get().getUserLevel());

            if(request.getActive()==null)
                request.setActive(resource.get().getActive());

            request.setCreatedOn((int) (new Date().getTime()/1000));
            request.setLastLogin((int) (new Date().getTime()/1000));

            UserRegister newUser = userRegisterRepo.save(request);
            if(newUser.getId()!=null){
                saveToUsersGroups(request);
                return newUser;
            }else{
                return null;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }

    @DeleteMapping(path = "/{id}")
    @ApiOperation("Delete User By Id")
    public boolean deleteUser(
        @RequestHeader("Authorization") String Authorization,
        @PathVariable Long id) {
        try {
            Optional<Users> resource = userRepo.findById(id);
            if(resource.isPresent()){
                userRepo.deleteById(id);
                return true;
            }else{
                return false;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
    }
}