package com.primasaver.api.controller;

import com.primasaver.api.dao.DeviceDAO;
import com.primasaver.api.dao.PanelDAO;
import com.primasaver.api.dao.ZonaDAO;
import com.primasaver.api.domain.Client;
import com.primasaver.api.domain.Device;
import com.primasaver.api.domain.QClient;
import com.primasaver.api.dto.ClientDTO;
import com.primasaver.api.dto.DeviceDTO;
import com.primasaver.api.dto.PanelDTO;
import com.primasaver.api.dto.ZonaDTO;
import com.primasaver.api.repository.ClientRepository;
import com.primasaver.api.repository.DeviceRepository;
import com.querydsl.core.BooleanBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/clients")
@Api(tags = "Client")
public class ClientController {
    @Autowired
    private ClientRepository clientRepo;

    @Autowired
    private DeviceDAO deviceDAO;

    @Autowired
    private PanelDAO panelDAO;

    @Autowired
    private ZonaDAO zonaDAO;

//    Get All Client
    @CrossOrigin
    @GetMapping(path = "")
    @ApiOperation("Get All Client")
    public Iterable<ClientDTO> getAllClient(
        @RequestHeader("Authorization") String Authorization,
        @RequestParam(name = "company", required = false) String company) {
            try {
                QClient client = QClient.client;
                BooleanBuilder builder = new BooleanBuilder();

                if (company != null) {
                    builder.and(client.company.likeIgnoreCase('%'+company +'%'));
                }
                Iterable<Client> clients = clientRepo.findAll(builder);
                List<ClientDTO> clientDTOList = new ArrayList<ClientDTO>();
                for (Client value:clients) {
                    ClientDTO clientDTO = new ClientDTO();

                    clientDTO.setIdClient(value.getIdClient());
                    clientDTO.setCompany(value.getCompany());
                    List<DeviceDTO> devices = deviceDAO.getDevice(value.getIdClient());
                    List<PanelDTO> panels = panelDAO.getPanel(value.getIdClient());
                    List<ZonaDTO> zonas = zonaDAO.getZona(value.getIdClient());
                    clientDTO.setDeviceList(devices);
                    clientDTO.setPanelList(panels);
                    clientDTO.setZonaList(zonas);
                    clientDTO.setClientAlias(value.getClientAlias());
                    clientDTO.setAddress(value.getAddress());
                    clientDTO.setPhone(value.getPhone());
                    clientDTO.setActive(value.getActive());
                    clientDTO.setLogo(value.getLogo());
                    clientDTO.setContractDate(value.getContractDate());
                    clientDTO.setLinkSite(value.getLinkSite());

                    clientDTOList.add(clientDTO);
                }
                return clientDTOList;
            } catch (Exception e) {
                throw e;
            }
    }


    //    Get All Client with Page
    @CrossOrigin
    @GetMapping(path = "/page")
    @ApiOperation("Get All Client with Page")
    public Page<Client> getClientByAttribute (
        @RequestHeader("Authorization") String Authorization,
        @RequestParam(name = "company", required = false) String company,
        Pageable pageRequest) {
            try {
                QClient client = QClient.client;
                BooleanBuilder builder = new BooleanBuilder();

                if (company != null) {
                    builder.and(client.company.likeIgnoreCase('%'+company +'%'));
                }
                return clientRepo.findAll(builder, pageRequest);
            } catch (Exception e) {
                throw e;
            }
    }

//    Get Client by ID
    @CrossOrigin
    @GetMapping(path = "/{id}")
    @ApiOperation("Get Client by Id")
    public Optional<Client> getClientById(
        @RequestHeader("Authorization") String Authorization,
        @PathVariable(value = "id") Long idClient) {
            try {
                return clientRepo.findById(idClient);
            } catch (Exception e) {
                throw e;
            }
    }

//    Create Client
    @CrossOrigin
    @PostMapping(path = "")
    @ApiOperation("Create New Client")
    public Client createClient(
        @RequestHeader("Authorization") String Authorization,
        @RequestBody Client client) {
            try {
                return clientRepo.save(client);
            } catch (Exception e) {
                throw e;
            }
    }

//    Update Client
    @CrossOrigin
    @PutMapping(path = "/{id}")
    @ApiOperation("Update Client by Id")
    public Client updateClientById(
        @RequestHeader("Authorization") String Authorization,
        @RequestBody Client client,
        @PathVariable("id") Long idClient) {
            try {
                client.setIdClient(idClient);
                return clientRepo.save(client);
            } catch (Exception e) {
                throw e;
            }
    }

// Delete Client
    @CrossOrigin
    @DeleteMapping(path = "/{id}")
    @ApiOperation("Delete Client by Id")
    public boolean deleteClientById(
        @RequestHeader("Authorization") String Authorization,
        @PathVariable("id") Long idClient) {
        Optional<Client> client= getClientById(Authorization, idClient);
        if (client != null) {
            clientRepo.deleteById(idClient);
        }
        return true;
    }

}
