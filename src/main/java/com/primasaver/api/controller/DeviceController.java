package com.primasaver.api.controller;

import com.primasaver.api.domain.Device;
import com.primasaver.api.domain.QDevice;
import com.primasaver.api.repository.DeviceRepository;
import com.querydsl.core.BooleanBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "/devices")
@Api(tags = "Device")
public class DeviceController {
    @Autowired
    private DeviceRepository deviceRepository;

//    Get All Devices
    @CrossOrigin
    @GetMapping(path = "")
    @ApiOperation("Get All Devices")
    public Iterable<Device> getDevice(
        @RequestParam(name = "device", required = false) String deviceIdentityId) {
        try {
            QDevice device = QDevice.device;
            BooleanBuilder builder = new BooleanBuilder();

            if (deviceIdentityId != null) {
                builder.and(device.deviceIdentityId.likeIgnoreCase('%' + deviceIdentityId + '%'));
            }
            return deviceRepository.findAll(builder);
        } catch (Exception e) {
            throw e;
        }
    }

//    Get All Devices With Page
    @CrossOrigin
    @GetMapping(path = "/page")
    @ApiOperation("Get All Devices With Page")
    public Page<Device> getDeviceByAttribute(
        @RequestParam(name = "device", required = false) String deviceIdentityId,
        Pageable pageRequest) {
        try {
            QDevice device = QDevice.device;
            BooleanBuilder builder = new BooleanBuilder();
            if (deviceIdentityId != null) {
                builder.and(device.deviceIdentityId.likeIgnoreCase("%" + deviceIdentityId + "%"));
            }
            return deviceRepository.findAll(builder, pageRequest);
        } catch (Exception e) {
            throw e;
        }
    }

//    Get Device By ID
    @CrossOrigin
    @GetMapping(path = "/{id}")
    @ApiOperation("Get Device By ID")
    public Optional<Device> getDeviceById(
        @PathVariable(name = "id") Long idDevice) {
        try {
            return deviceRepository.findById(idDevice);
        } catch (Exception e) {
            throw e;
        }
    }
}
