package com.primasaver.api.controller;

import com.primasaver.api.domain.QZona;
import com.primasaver.api.domain.Zona;
import com.primasaver.api.repository.ZonaRepository;
import com.querydsl.core.BooleanBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "/zonas")
@Api(tags = "Zona Location")
public class ZonaController {
    @Autowired
    private ZonaRepository zonaRepo;

//    Get all zona
    @CrossOrigin
    @GetMapping(path = "")
    @ApiOperation("Get All Zona Location")
    public Iterable<Zona> getZona(
        @RequestParam(name = "zona_name", required = false) String zonaName) {
        try {
            QZona zona = QZona.zona;
            BooleanBuilder builder = new BooleanBuilder();

            if (zonaName != null) {
                builder.and(zona.zonaName.likeIgnoreCase('%' + zonaName + '%'));
            }
            return zonaRepo.findAll(builder);
        } catch (Exception e) {
            throw e;
        }
    }

//    Get all zona with page
    @CrossOrigin
    @GetMapping(path = "/page")
    @ApiOperation("Get All Zona Location With Page")
    public Page<Zona> getZonaByAttribute(
        @RequestParam(name = "zona_name", required = false) String zonaName, Pageable pageRequest) {
        try {
            QZona zona = QZona.zona;
            BooleanBuilder builder = new BooleanBuilder();
            if (zonaName != null) {
                builder.and(zona.zonaName.likeIgnoreCase("%" + zonaName + "%"));
            }
            return zonaRepo.findAll(builder, pageRequest);
        }catch (Exception e) {
            throw e;
        }
    }

//    Get zona by id
    @CrossOrigin
    @GetMapping(path = "/{id}")
    @ApiOperation("Get Device By ID")
    public Optional<Zona> getDeviceById(
            @PathVariable(name = "id") Long idZona) {
        try {
            return zonaRepo.findById(idZona);
        } catch (Exception e) {
            throw e;
        }
    }
}
