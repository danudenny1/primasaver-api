package com.primasaver.api.controller;

import com.google.common.collect.Lists;
import com.primasaver.api.domain.ExpiredToken;
import com.primasaver.api.domain.QExpiredToken;
import com.primasaver.api.repository.ExpiredTokenRepository;
import com.primasaver.api.repository.RoleGlobalRepository;
import com.primasaver.api.repository.UserRepository;
import com.primasaver.api.repository.UserRoleRepository;
import com.primasaver.api.security.JwtAuthenticationRequest;
import com.primasaver.api.security.JwtTokenUtil;
import com.primasaver.api.security.JwtUser;
import com.primasaver.api.service.JwtAuthenticationResponse;
import com.querydsl.core.BooleanBuilder;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Objects;

@RestController
@RequestMapping(path = "/auth")
@Api(tags = "Auth")
@Slf4j
public class AuthenticationController {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    @Qualifier("jwtUserDetailsService")
    private UserDetailsService userDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private RoleGlobalRepository roleGlobalRepository;

    @Autowired
    private ExpiredTokenRepository expiredTokenRepository;

    @PostMapping(value = "/login")
    public ResponseEntity<?> createAuthenticationToken(
            @RequestBody JwtAuthenticationRequest authenticationRequest) throws AuthenticationException {

        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String kode = jwtTokenUtil.generateToken(userDetails);

        // Return the token
        return ResponseEntity.ok(new JwtAuthenticationResponse(kode, userDetails));
    }

    @PostMapping(value = "/logout")
    public ExpiredToken destroyAuthenticationToken(@RequestHeader("Authorization") String Authorization){
        ExpiredToken token = new ExpiredToken();
        token.setToken(Authorization);

        LocalDateTime now = LocalDateTime.now();
        token.setTimestamp(now);

        try {
            return expiredTokenRepository.save(token);
        } catch (Exception e) {
            throw e;
        }

    }

    @GetMapping(value = "/is_expired")
    public Boolean isLoggedOut(@RequestHeader("Authorization") String Authorization){
        try {
            QExpiredToken expiredToken = QExpiredToken.expiredToken;
            BooleanBuilder builder = new BooleanBuilder();

            builder.and(expiredToken.token.eq(Authorization));

            Iterable<ExpiredTokenRepository> result = expiredTokenRepository.findAll(builder);
            Integer arrayLength = Lists.newArrayList(result).size();

            if (arrayLength == 0 ){
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            throw e;
        }

    }

    @GetMapping(value = "${jwt.route.authentication.refresh}")
    public void refreshAndGetAuthenticationToken(
            @RequestHeader("Authorization") String Authorization,
            HttpServletRequest request) {
        String authToken = request.getHeader(tokenHeader);
        final String token = authToken;
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);
    }

    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<String> handleAuthenticationException(AuthenticationException e) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
    }

    private void authenticate(String username, String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new AuthenticationException("User belum diaktivasi!", e);
        } catch (BadCredentialsException e) {
            throw new AuthenticationException("User atau Password salah!", e);
        }
    }
}

