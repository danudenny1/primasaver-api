package com.primasaver.api.controller;

import com.primasaver.api.domain.Panel;
import com.primasaver.api.domain.QPanel;
import com.primasaver.api.repository.PanelRepository;
import com.querydsl.core.BooleanBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/panels")
@Api(tags = "Panel Location")
public class PanelController {
    @Autowired
    private PanelRepository panelRepo;

//    Get All Panel Location
    @CrossOrigin
    @GetMapping(path = "")
    @ApiOperation("Get All Panel Location")
    public Iterable<Panel> getPanel(
        @RequestParam(name = "panel_name", required = false) String panelName) {
        try {
            QPanel panel = QPanel.panel;
            BooleanBuilder builder = new BooleanBuilder();

            if (panelName != null) {
                builder.and(panel.panelName.likeIgnoreCase('%' + panelName + '%'));
            }

            return panelRepo.findAll(builder);
        } catch (Exception e) {
            throw e;
        }
    }
}
