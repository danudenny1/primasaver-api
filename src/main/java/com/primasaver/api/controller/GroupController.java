package com.primasaver.api.controller;

import com.primasaver.api.domain.Groups;
import com.primasaver.api.domain.QGroups;
import com.primasaver.api.repository.GroupsRepository;
import com.querydsl.core.BooleanBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/groups")
@Api(tags = "Groups")
public class GroupController {

    @Autowired
    private GroupsRepository groupsRepository;

    @GetMapping(path = "")
    @ApiOperation("Get Users")
    public Iterable<Groups> getUsers(
            @RequestHeader("Authorization") String Authorization) {
        try {
            QGroups groups = QGroups.groups;
            BooleanBuilder builder = new BooleanBuilder();

            Iterable<Groups> all = groupsRepository.findAll(builder);

            return all;
        } catch (Exception e) {
            throw e;
        }
    }


}
