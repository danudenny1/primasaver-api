package com.primasaver.api.controller;

import com.primasaver.api.service.StorageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/uploads")
@Api(tags = "Upload")
public class UploadController {
    @Autowired
    StorageService storageService;

    List<String> files = new ArrayList<String>();

    @CrossOrigin
    @ApiOperation("Upload File / Photos")
    @PostMapping("/post")
    public ResponseEntity<String> handleFileUpload(
        @RequestHeader("Authorization") String Authorization,
        @RequestParam("file") MultipartFile file) {
            String message = "";
            try {
                storageService.store(file);
                files.add(file.getOriginalFilename());

                message = "You successfully uploaded " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.OK).body(message);
            } catch (Exception e) {
                message = "FAIL to upload " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
            }
    }

    @CrossOrigin
    @ApiOperation("Get File / Photos")
    @GetMapping("/getallfiles")
    public ResponseEntity<List<String>> getListFiles(
        @RequestHeader("Authorization") String Authorization,
        Model model) {
            List<String> fileNames = files
                    .stream().map(fileName -> MvcUriComponentsBuilder
                            .fromMethodName(UploadController.class, "getFile", fileName).build().toString())
                    .collect(Collectors.toList());

            return ResponseEntity.ok().body(fileNames);
    }

    @CrossOrigin
    @ApiOperation("Get File / Photos by name")
    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(
        @RequestHeader("Authorization") String Authorization,
        @PathVariable String filename) {
            Resource file = storageService.loadFile(filename);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                    .body(file);
    }
}