package com.primasaver.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "account")
@Getter
@Setter
@NoArgsConstructor
public class UserDTO {
    @Id
    private Long idAccount;

    @Column
    private String username;
    private String fullName;
    private String password;
    private String foto;
    private String email;
    private String phone;
    private String groupId;
    private Boolean active;
    private LocalDateTime joinedDate;
    private String pass;

    @Transient
    private Long roleId;
    @Transient
    private String roleIdText;
}
