package com.primasaver.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "users")
@Setter
@Getter
@NoArgsConstructor
public class UsersDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_id_seq")
    @SequenceGenerator(name = "users_id_seq", sequenceName = "users_id_seq", allocationSize = 1)
    private Long id;

    @Column
    private String username;
    private String email;
    private String firstName;
    private String lastName;
    private Integer active;
    private Integer userLevel;
    private String phone;
    private String company;

    @Transient
    private Long groupId;
    @Transient
    private String groupIdText;
    @Transient
    private String userLevelText;
}
