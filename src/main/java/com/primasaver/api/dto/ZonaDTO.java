package com.primasaver.api.dto;

import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
public class ZonaDTO {
    public Long idZona;
    public Long clientId;
    public String zonaName;
    public Float longitude;
    public Float latitude;
}
