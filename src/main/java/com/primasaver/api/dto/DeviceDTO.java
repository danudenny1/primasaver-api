package com.primasaver.api.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Setter
@Getter
@NoArgsConstructor
public class DeviceDTO {
    private Long idDevice;
    private Long clientId;
    private Long catId;
    private Long panelId;
    private String deviceIdentityId;
    private String inOut;
    private String status;
}
