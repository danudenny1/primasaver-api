package com.primasaver.api.dto;

import com.primasaver.api.domain.Device;
import lombok.*;

import java.util.List;

@Data
@Getter
@Setter
//@ToString
@NoArgsConstructor
public class ClientDTO {
    private Long idClient;
    private String company;
    private List<DeviceDTO> deviceList;
    private List<PanelDTO> panelList;
    private List<ZonaDTO> zonaList;
    private String clientAlias;
    private String address;
    private String phone;
    private String logo;
    private String contractDate;
    private Boolean active;
    private String linkSite;

}
