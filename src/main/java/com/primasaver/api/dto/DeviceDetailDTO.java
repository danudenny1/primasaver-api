package com.primasaver.api.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@NoArgsConstructor
public class DeviceDetailDTO {
    private String company;
    private String deviceIdentityId;
    private String inOut;
    private String status;
    private String category;
    private String zonaName;
    private Boolean panelName;
}
