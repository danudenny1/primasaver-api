package com.primasaver.api.dto;


import lombok.*;

@Data
@Getter
@Setter
@ToString
@NoArgsConstructor
public class UsersGroupsDTO {
    private Long userId;
    private String username;
    private String email;
    private String firstName;
    private String lastName;
    private Integer active;
    private Integer userLevel;
    private Long groupId;
    private String groupIdText;
    private String userLevelText;
}
