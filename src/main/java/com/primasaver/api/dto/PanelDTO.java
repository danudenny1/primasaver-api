package com.primasaver.api.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Data
@Getter
@Setter
@NoArgsConstructor
public class PanelDTO {
    public Long idPanel;
    public Long clientId;
    public String panelName;
    public String serialNumber;
    public String panelDesc;
    public Long capacity;
    public LocalDateTime startReport;
    public Long zonaId;
    public LocalDateTime installedDate;
    public Long capacityTolerance;
}
