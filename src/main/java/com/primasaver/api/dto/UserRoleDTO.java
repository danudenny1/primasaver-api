package com.primasaver.api.dto;

import lombok.*;

import java.time.LocalDateTime;

@Data
@Getter
@Setter
@NoArgsConstructor
@ToString
public class UserRoleDTO {
    private Long userId;
    private String username;
    private String fullName;
    private String password;
    private String foto;
    private String email;
    private String phone;
    private String groupId;
    private Boolean active;
    private LocalDateTime joinedDate;
    private String pass;
    private Long roleId;
    private String roleIdText;
}
