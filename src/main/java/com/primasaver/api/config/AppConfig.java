package com.primasaver.api.config;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan("com.primasaver.api")
@EnableJpaRepositories("com.primasaver.api.repository")
public class AppConfig {
    @Autowired
    public Environment env;

    @Bean
    public DataSource dataSource() {
        DataSource dataSource = new DataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://" + env.getProperty("db-host") + ":" + env.getProperty("db-port") + "/" + env.getProperty("db-name"));
        dataSource.setUsername(env.getProperty("db-user"));
        dataSource.setPassword(env.getProperty("db-pass"));
        dataSource.setValidationQuery("SELECT 1");
        dataSource.setValidationInterval(1 * 60 * 1000);
        dataSource.setInitialSize(1);
        dataSource.setTestOnBorrow(true);
        return dataSource;
    }
}
