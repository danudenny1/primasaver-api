package com.primasaver.api.dao;

import com.primasaver.api.dto.DeviceDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
@Slf4j
public class DeviceDAO {
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<DeviceDTO> getDevice(Long clientId) {
        String where = "WHERE client_id = "+clientId;
        String sql = "SELECT * FROM device ";
        if(clientId!=null){
            sql = sql+" "+where;
        }
        try{
            List<DeviceDTO> Device = namedParameterJdbcTemplate.query(
                sql, new RowMapper<DeviceDTO>() {
                    @Override
                    public DeviceDTO mapRow(ResultSet rs, int i) throws SQLException {
                        DeviceDTO deviceDTO = new DeviceDTO();
                        deviceDTO.setIdDevice(rs.getLong("id_device"));
                        deviceDTO.setClientId(rs.getLong("client_id"));
                        deviceDTO.setCatId(rs.getLong("cat_id"));
                        deviceDTO.setPanelId(rs.getLong("panel_id"));
                        deviceDTO.setDeviceIdentityId(rs.getString("device_identity_id"));
                        deviceDTO.setInOut(rs.getString("in_out"));
                        deviceDTO.setStatus(rs.getString("status"));

                        return deviceDTO;
                    }
                }
            );
            return Device;
        } catch (EmptyResultDataAccessException ex) {
            return new ArrayList<DeviceDTO>();
        }
    }
}
