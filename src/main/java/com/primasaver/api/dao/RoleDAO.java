package com.primasaver.api.dao;

import com.primasaver.api.dto.RoleDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
@Slf4j
public class RoleDAO {
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<RoleDTO> getGroup() {
        String sql = "SELECT * FROM account_group";
        try{
            List<RoleDTO> Role = namedParameterJdbcTemplate.query(
                    sql, new RowMapper<RoleDTO>() {
                        @Override
                        public RoleDTO mapRow(ResultSet res, int i) throws SQLException {
                            RoleDTO roleDTO = new RoleDTO();
                            roleDTO.setIdGroup(res.getLong("id_group"));
                            roleDTO.setGroupDesc(res.getString("group_desc"));
                            return roleDTO;
                        }
                    }
            );
            return Role;
        } catch (EmptyResultDataAccessException ex) {
            return new ArrayList<RoleDTO>();
        }
    }
}
