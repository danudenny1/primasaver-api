package com.primasaver.api.dao;

import com.primasaver.api.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

@Repository
@Slf4j
public class UserDAO {
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<UserDTO> getUser() {
        String sql = "SELECT * FROM account";
        try{
            List<UserDTO> User = namedParameterJdbcTemplate.query(
                sql, new RowMapper<UserDTO>() {
                    @Override
                    public UserDTO mapRow(ResultSet resultSet, int i) throws SQLException {
                        UserDTO userDTO = new UserDTO();
                        userDTO.setIdAccount(resultSet.getLong("id_account"));
                        userDTO.setUsername(resultSet.getString("username"));
                        userDTO.setFullName(resultSet.getString("full_name"));
                        userDTO.setPassword(resultSet.getString("password"));
                        userDTO.setFoto(resultSet.getString("foto"));
                        userDTO.setEmail(resultSet.getString("email"));
                        userDTO.setPhone(resultSet.getString("phone"));
                        userDTO.setGroupId(resultSet.getString("group_id"));
                        userDTO.setActive(resultSet.getBoolean("active"));
                        if (resultSet.getTimestamp("joined_date") != null) {
                            userDTO.setJoinedDate(LocalDateTime.ofInstant(
                                    Instant.ofEpochMilli(resultSet.getTimestamp("joined_date").getTime()),
                                    ZoneOffset.UTC
                            ));
                        }
                        userDTO.setPass(resultSet.getString("pass"));
                        return userDTO;
                    }
            }
            );
            return User;
        } catch (EmptyResultDataAccessException ex) {
            return new ArrayList<UserDTO>();
        }
    }
}
