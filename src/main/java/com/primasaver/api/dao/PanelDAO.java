package com.primasaver.api.dao;

import com.primasaver.api.dto.PanelDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

@Repository
@Slf4j
public class PanelDAO {
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<PanelDTO> getPanel(Long clientId) {
        String where = "WHERE client_id = "+clientId;
        String sql = "SELECT * FROM panel_location ";
        if(clientId!=null){
            sql = sql+" "+where;
        }
        try {
            List<PanelDTO> Panel = namedParameterJdbcTemplate.query(
                sql, new RowMapper<PanelDTO>() {
                    @Override
                    public PanelDTO mapRow(ResultSet res, int i) throws SQLException {
                        PanelDTO panelDTO = new PanelDTO();
                        panelDTO.setIdPanel(res.getLong("id_panel"));
                        panelDTO.setClientId(res.getLong("client_id"));
                        panelDTO.setPanelName(res.getString("panel_name"));
                        panelDTO.setPanelDesc(res.getString("panel_desc"));
                        panelDTO.setSerialNumber(res.getString("serial_number"));
                        panelDTO.setCapacity(res.getLong("capacity"));
                        if (res.getTimestamp("start_report") != null) {
                            panelDTO.setStartReport(LocalDateTime.ofInstant(
                                    Instant.ofEpochMilli(res.getTimestamp("start_report").getTime()),
                                    ZoneOffset.UTC
                            ));
                        }
                        panelDTO.setZonaId(res.getLong("zona_id"));
                        panelDTO.setCapacityTolerance(res.getLong("capacity_tolerance"));
                        if (res.getTimestamp("installed_date") != null) {
                            panelDTO.setInstalledDate(LocalDateTime.ofInstant(
                                    Instant.ofEpochMilli(res.getTimestamp("installed_date").getTime()),
                                    ZoneOffset.UTC
                            ));
                        }

                        return panelDTO;
                    }
                }
            );
            return Panel;
        } catch (EmptyResultDataAccessException ex) {
            throw ex;
        }
    }
}
