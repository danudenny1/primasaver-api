package com.primasaver.api.dao;

//import com.primasaver.api.dto.ClientDTO;
import com.primasaver.api.dto.ClientDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
@Slf4j
public class ClientDAO {
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<ClientDTO> getClient() {

        String sql = "SELECT * FROM client";
        try {
            List<ClientDTO> Client = namedParameterJdbcTemplate.query(
                    sql, new RowMapper<ClientDTO>() {
                        @Override
                        public ClientDTO mapRow(ResultSet resultSet, int i) throws SQLException {
                            ClientDTO clientDTO = new ClientDTO();
                            clientDTO.setIdClient(resultSet.getLong("id_client"));
                            clientDTO.setCompany(resultSet.getString("company"));
                            clientDTO.setClientAlias(resultSet.getString("client_alias"));
                            clientDTO.setAddress(resultSet.getString("address"));
                            clientDTO.setPhone(resultSet.getString("phone"));
                            clientDTO.setContractDate(resultSet.getString("contract_date"));
                            clientDTO.setLogo(resultSet.getString("logo"));
                            clientDTO.setActive(resultSet.getBoolean("active"));
                            clientDTO.setLinkSite(resultSet.getString("link_site"));

                            return clientDTO;
                        }
                    }
            );
            return Client;
        } catch (EmptyResultDataAccessException ex) {
            return new ArrayList<ClientDTO>();
        }
    }
}
