package com.primasaver.api.dao;

import com.primasaver.api.dto.UserRoleDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Slf4j
public class UserRoleDAO {
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<UserRoleDTO> getUserRole(Long roleId) {
        Map<String, Object> paramMap = new HashMap<String, Object>();

        String where = "";
        if(roleId != null)
            where += " and ur.role_id = '"+roleId+"' ";

            String sql = "SELECT " +
                "ac.*, " +
                "ag.group_desc, " +
                "FROM user_role ur " +
                "LEFT JOIN account_group ag ON (ag.id_group = ur.role_id) " +
                "LEFT JOIN account ac ON (ac.id_account = ur .user_id)" +
            where;

        System.out.println(sql);

        try {
            List<UserRoleDTO> userRoles = namedParameterJdbcTemplate.query(
                    sql, new RowMapper<UserRoleDTO>() {

                        @Override
                        public UserRoleDTO mapRow(ResultSet set, int i) throws SQLException {

                            UserRoleDTO userRoles = new UserRoleDTO();

                            userRoles.setUserId(set.getLong("user_id"));
                            userRoles.setUsername(set.getString("username"));
                            userRoles.setPassword(set.getString("password"));
                            userRoles.setFullName(set.getString("full_name"));
                            userRoles.setFoto(set.getString("foto"));
                            userRoles.setEmail(set.getString("email"));
                            userRoles.setPhone(set.getString("phone"));
                            userRoles.setGroupId(set.getString("group_id"));
                            userRoles.setActive(set.getBoolean("active"));
                            if (set.getTimestamp("joined_date") != null) {
                                userRoles.setJoinedDate(LocalDateTime.ofInstant(
                                        Instant.ofEpochMilli(set.getTimestamp("joined_date").getTime()),
                                        ZoneOffset.UTC
                                ));
                            }
                            userRoles.setPass(set.getString("pass"));
                            userRoles.setRoleId(set.getLong("role_id"));
                            userRoles.setRoleIdText(set.getString("group_desc"));

                            return userRoles;
                        }
                    });

            return userRoles;
        }  catch (EmptyResultDataAccessException ex) {
            return new ArrayList<UserRoleDTO>();
        }
    }
}
