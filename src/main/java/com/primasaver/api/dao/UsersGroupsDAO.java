package com.primasaver.api.dao;

import com.primasaver.api.dto.UsersGroupsDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Slf4j

public class UsersGroupsDAO {
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<UsersGroupsDTO> getUsersGroups(Long groupId){
        Map<String, Object> paramMap = new HashMap<String, Object>();

        String where = "";
        if(groupId != null)
            where += " and ug.group_id = '"+groupId+"' ";

            String sql = "SELECT " +
                "u.id as user_id, " +
                "u.username, " +
                "u.email, " +
                "u.first_name, " +
                "u.last_name, " +
                "u.active, " +
                "u.user_level, " +
                "ug.group_id, " +
                "g.name, " +
                "g.description " +
                "FROM users_groups ug " +
                "LEFT JOIN groups g ON (g.id=ug.group_id) " +
                "LEFT JOIN users u ON (ug.user_id=u.id)" +
                "WHERE '1'" +
            where;

        System.out.println("sql = "+sql);

        try {
            List<UsersGroupsDTO> usersGroups = namedParameterJdbcTemplate.query(
                sql, new RowMapper<UsersGroupsDTO>() {

                    @Override
                    public UsersGroupsDTO mapRow(ResultSet set, int i) throws SQLException {

                        UsersGroupsDTO usersGroups = new UsersGroupsDTO();

                        usersGroups.setUserId(set.getLong("user_id"));
                        usersGroups.setUsername(set.getString("username"));
                        usersGroups.setEmail(set.getString("email"));
                        usersGroups.setFirstName(set.getString("first_name"));
                        usersGroups.setLastName(set.getString("last_name"));
                        usersGroups.setActive(set.getInt("active"));
                        usersGroups.setGroupId(set.getLong("group_id"));
                        usersGroups.setGroupIdText(set.getString("description"));
                        usersGroups.setUserLevel(set.getInt("user_level"));

                        return usersGroups;
                    }
                });

            return usersGroups;
        }  catch (EmptyResultDataAccessException ex) {
            return new ArrayList<UsersGroupsDTO>();
        }
    }
}
