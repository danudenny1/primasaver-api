package com.primasaver.api.dao;

import com.primasaver.api.dto.ZonaDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
@Slf4j
public class ZonaDAO {
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<ZonaDTO> getZona(Long clientId) {
        String where = "WHERE client_id = "+clientId;
        String sql = "SELECT * FROM zona_location ";
        if(clientId!=null){
            sql = sql+" "+where;
        }
        try {
            List<ZonaDTO> Zona = namedParameterJdbcTemplate.query(
                sql, new RowMapper<ZonaDTO>() {
                    @Override
                    public ZonaDTO mapRow(ResultSet res, int i) throws SQLException {
                        ZonaDTO zonaDTO = new ZonaDTO();
                        zonaDTO.setIdZona(res.getLong("id_zona"));
                        zonaDTO.setClientId(res.getLong("client_id"));
                        zonaDTO.setZonaName(res.getString("zona_name"));
                        zonaDTO.setLongitude(res.getFloat("longitude"));
                        zonaDTO.setLatitude(res.getFloat("latitude"));
                        return zonaDTO;
                    }
                }
            );
            return Zona;
        } catch (EmptyResultDataAccessException ex) {
            return new ArrayList<ZonaDTO>();
        }
    }
}
