package com.primasaver.api.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "expired_token")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ExpiredToken {
    @Id
    @Column(length = 1024)
    private String token;

    @Column
    private LocalDateTime timestamp;

}

