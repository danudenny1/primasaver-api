package com.primasaver.api.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;

@Entity
@Table(name = "users_groups")
@Setter
@Getter
@NoArgsConstructor
@DynamicInsert
public class UsersGroups {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_groups_id_seq")
    @SequenceGenerator(name = "users_groups_id_seq", sequenceName = "users_groups_id_seq", allocationSize = 1)
    private Long id;

    @Column
    private Long groupId;
    private Long userId;
}
