package com.primasaver.api.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name= "client")
@Setter
@Getter
@NoArgsConstructor
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idClient;

    @Column
    private String company;
    private String clientAlias;
    private String address;
    private String phone;
    private String logo;
    private String contractDate;
    private Boolean active;
    private String linkSite;

}
