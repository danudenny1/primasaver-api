package com.primasaver.api.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "users")

@Setter
@Getter
@NoArgsConstructor
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
//    @NotNull(message = "error.username.notnull")
//    @Size(min = 1, max = 30, message = "error.username.size")
    private String username;
    private String password;
    private String passdev;
    private String ipAddress;
    private String email;
    private String activationCode;
    private String salt;
    private String rememberCode;
    private Integer createdOn;
    private Integer lastLogin;
    private Integer active;
    private String firstName;
    private String lastName;
    private String company;
//    private String forgottenPasswordTime;
//    private Integer forgottenPasswordCode;
    private String phone;
    private Integer userLevel;
    private Date lastPasswordResetDate;

    @Transient
    private String userLevelText;

    @Transient
    private String mailSubject;

    @Transient
    private Long groupId;

    @Transient
    private String mailText;

    @Transient
    private String groupIdText;

    private Boolean isEnabled;
    private String getAuthority;

    public Boolean isEnabled() {
        return getActive() == 1;
    }

    public String getAuthority(){
        Integer auth = getUserLevel();
        return Authorities.getById(auth);
    }
}