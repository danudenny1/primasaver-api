package com.primasaver.api.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "panel_location")
@Getter
@Setter
@NoArgsConstructor
public class Panel {
    @Id
    public Long idPanel;

    @Column
    public Long clientId;
    public String panelName;
    public String serialNumber;
    public String panelDesc;
    public Long capacity;
    public LocalDateTime startReport;
    public Long zonaId;
    public LocalDateTime installedDate;
    public Long capacityTolerance;
}
