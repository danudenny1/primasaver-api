package com.primasaver.api.domain;

public enum Authorities {

    SUPER_ADMIN(0, "Super Administrator"),
    USER_ADMIN(1, "User Administrator"),
    USER_CLIENT(2, "User CLient");

    public static String getById(Integer id) {
        for(Authorities e : values()) {
            if(e.id.equals(id)) return e.name;
        }
        return "USER_ADMIN";
    }

    private String name;
    private Integer id;

    Authorities(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

}
