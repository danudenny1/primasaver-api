package com.primasaver.api.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "zona_location")
@Getter
@Setter
@NoArgsConstructor
public class Zona {
    @Id
    public Long idZona;

    @Column
    public Long clientId;
    public String zonaName;
    public Float longitude;
    public Float latitude;
}
