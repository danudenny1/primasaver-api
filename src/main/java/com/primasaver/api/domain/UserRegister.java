package com.primasaver.api.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "users")

@Setter
@Getter
@NoArgsConstructor
public class UserRegister {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_id_seq")
    @SequenceGenerator(name = "users_id_seq", sequenceName = "users_id_seq", allocationSize = 1)
    private Long id;

    @Column
    private String username;
    private String password;
    private String passdev;
    private String email;
    private Integer createdOn;
    private Integer lastLogin;
    private Integer active;
    private String firstName;
    private String lastName;
    private String company;
    private String phone;
    private Integer userLevel;
//
//    @Transient
//    private Long groupId;
    @Transient
    private String userLevelText;
}
