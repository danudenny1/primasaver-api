package com.primasaver.api.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "device")
@Setter
@Getter
@NoArgsConstructor
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDevice;

    @Column
    private Long clientId;
    private Long cat_id;
    private Long panel_id;
    private String deviceIdentityId;
    private String inOut;
    private String status;
}
