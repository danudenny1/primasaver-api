package com.primasaver.api.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.ModelMap;

import java.io.Serializable;

public class JwtAuthenticationResponse implements Serializable {

    private static final long serialVersionUID = 1250166508152483573L;
    private final String token;
    private UserDetails user;

    public JwtAuthenticationResponse(String token, UserDetails user) {
        this.token = token;
        this.user = user;
    }

    public String getToken() { return this.token; }
    public UserDetails getUser() { return this.user; }

}
