package com.primasaver.api;

import com.primasaver.api.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;


@SpringBootApplication
@Slf4j
public class PrimasaverApiApplication implements CommandLineRunner {

    @Resource
    StorageService storageService;

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(PrimasaverApiApplication.class);
//		app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
		log.warn("Primasaver API is start on http://localhost:8333/");
	}

	@Override
	public void run(String... arg) throws Exception {
		storageService.deleteAll();
		storageService.init();
	}

}
