package com.primasaver.api.security;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.primasaver.api.domain.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public final class JwtUserFactory {

    private JwtUserFactory() {
    }

    public static JwtUser create(User user) {
        return new JwtUser(
                user.getId(),
                user.getUsername(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getPassword(),
                mapToGrantedAuthorities(user.getAuthority()),
                user.getActive(),
                user.isEnabled(),
                user.getLastPasswordResetDate(),
                user.getCompany(),
                user.getPhone()
        );
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(String authority) {
        return  new ArrayList<>(Collections.singletonList( new SimpleGrantedAuthority(authority)));
    }
}
