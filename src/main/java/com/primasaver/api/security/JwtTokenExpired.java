package com.primasaver.api.security;

import com.google.common.collect.Lists;
import com.primasaver.api.domain.QExpiredToken;
import com.primasaver.api.repository.ExpiredTokenRepository;
import com.querydsl.core.BooleanBuilder;
import org.springframework.beans.factory.annotation.Autowired;

public class JwtTokenExpired {
    @Autowired
    private ExpiredTokenRepository expiredTokenRepository;

    public Boolean checkIfTokenExpired(String authorization){
        try {
            QExpiredToken expiredToken = QExpiredToken.expiredToken;
            BooleanBuilder builder = new BooleanBuilder();

            builder.and(expiredToken.token.eq(authorization));

            Iterable<ExpiredTokenRepository> result = expiredTokenRepository.findAll(builder);
            Integer arrayLength = Lists.newArrayList(result).size();

            if (arrayLength == 0 ){
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            throw e;
        }

    }
}

