package com.primasaver.api.dto;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QUsersDTO is a Querydsl query type for UsersDTO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUsersDTO extends EntityPathBase<UsersDTO> {

    private static final long serialVersionUID = 776332513L;

    public static final QUsersDTO usersDTO = new QUsersDTO("usersDTO");

    public final NumberPath<Integer> active = createNumber("active", Integer.class);

    public final StringPath company = createString("company");

    public final StringPath email = createString("email");

    public final StringPath firstName = createString("firstName");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath lastName = createString("lastName");

    public final StringPath phone = createString("phone");

    public final NumberPath<Integer> userLevel = createNumber("userLevel", Integer.class);

    public final StringPath username = createString("username");

    public QUsersDTO(String variable) {
        super(UsersDTO.class, forVariable(variable));
    }

    public QUsersDTO(Path<? extends UsersDTO> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUsersDTO(PathMetadata metadata) {
        super(UsersDTO.class, metadata);
    }

}

