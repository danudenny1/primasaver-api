package com.primasaver.api.dto;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QUserDTO is a Querydsl query type for UserDTO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUserDTO extends EntityPathBase<UserDTO> {

    private static final long serialVersionUID = 302092970L;

    public static final QUserDTO userDTO = new QUserDTO("userDTO");

    public final BooleanPath active = createBoolean("active");

    public final StringPath email = createString("email");

    public final StringPath foto = createString("foto");

    public final StringPath fullName = createString("fullName");

    public final StringPath groupId = createString("groupId");

    public final NumberPath<Long> idAccount = createNumber("idAccount", Long.class);

    public final DateTimePath<java.time.LocalDateTime> joinedDate = createDateTime("joinedDate", java.time.LocalDateTime.class);

    public final StringPath pass = createString("pass");

    public final StringPath password = createString("password");

    public final StringPath phone = createString("phone");

    public final StringPath username = createString("username");

    public QUserDTO(String variable) {
        super(UserDTO.class, forVariable(variable));
    }

    public QUserDTO(Path<? extends UserDTO> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUserDTO(PathMetadata metadata) {
        super(UserDTO.class, metadata);
    }

}

