package com.primasaver.api.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QZona is a Querydsl query type for Zona
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QZona extends EntityPathBase<Zona> {

    private static final long serialVersionUID = -330791817L;

    public static final QZona zona = new QZona("zona");

    public final NumberPath<Long> clientId = createNumber("clientId", Long.class);

    public final NumberPath<Long> idZona = createNumber("idZona", Long.class);

    public final NumberPath<Float> latitude = createNumber("latitude", Float.class);

    public final NumberPath<Float> longitude = createNumber("longitude", Float.class);

    public final StringPath zonaName = createString("zonaName");

    public QZona(String variable) {
        super(Zona.class, forVariable(variable));
    }

    public QZona(Path<? extends Zona> path) {
        super(path.getType(), path.getMetadata());
    }

    public QZona(PathMetadata metadata) {
        super(Zona.class, metadata);
    }

}

