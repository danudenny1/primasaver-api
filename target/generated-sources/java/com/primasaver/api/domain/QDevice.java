package com.primasaver.api.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDevice is a Querydsl query type for Device
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDevice extends EntityPathBase<Device> {

    private static final long serialVersionUID = -702183579L;

    public static final QDevice device = new QDevice("device");

    public final NumberPath<Long> cat_id = createNumber("cat_id", Long.class);

    public final NumberPath<Long> clientId = createNumber("clientId", Long.class);

    public final StringPath deviceIdentityId = createString("deviceIdentityId");

    public final NumberPath<Long> idDevice = createNumber("idDevice", Long.class);

    public final StringPath inOut = createString("inOut");

    public final NumberPath<Long> panel_id = createNumber("panel_id", Long.class);

    public final StringPath status = createString("status");

    public QDevice(String variable) {
        super(Device.class, forVariable(variable));
    }

    public QDevice(Path<? extends Device> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDevice(PathMetadata metadata) {
        super(Device.class, metadata);
    }

}

