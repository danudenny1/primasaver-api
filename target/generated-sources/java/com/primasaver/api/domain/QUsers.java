package com.primasaver.api.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QUsers is a Querydsl query type for Users
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUsers extends EntityPathBase<Users> {

    private static final long serialVersionUID = -1669118183L;

    public static final QUsers users = new QUsers("users");

    public final StringPath activationCode = createString("activationCode");

    public final NumberPath<Integer> active = createNumber("active", Integer.class);

    public final StringPath company = createString("company");

    public final NumberPath<Integer> createdOn = createNumber("createdOn", Integer.class);

    public final StringPath email = createString("email");

    public final StringPath firstName = createString("firstName");

    public final StringPath getAuthority = createString("getAuthority");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath ipAddress = createString("ipAddress");

    public final BooleanPath isEnabled = createBoolean("isEnabled");

    public final NumberPath<Integer> lastLogin = createNumber("lastLogin", Integer.class);

    public final StringPath lastName = createString("lastName");

    public final DatePath<java.sql.Date> lastPasswordResetDate = createDate("lastPasswordResetDate", java.sql.Date.class);

    public final StringPath passdev = createString("passdev");

    public final StringPath password = createString("password");

    public final StringPath phone = createString("phone");

    public final StringPath rememberCode = createString("rememberCode");

    public final StringPath salt = createString("salt");

    public final NumberPath<Integer> userLevel = createNumber("userLevel", Integer.class);

    public final StringPath username = createString("username");

    public QUsers(String variable) {
        super(Users.class, forVariable(variable));
    }

    public QUsers(Path<? extends Users> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUsers(PathMetadata metadata) {
        super(Users.class, metadata);
    }

}

