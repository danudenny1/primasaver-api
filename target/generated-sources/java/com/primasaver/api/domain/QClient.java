package com.primasaver.api.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QClient is a Querydsl query type for Client
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QClient extends EntityPathBase<Client> {

    private static final long serialVersionUID = -724738854L;

    public static final QClient client = new QClient("client");

    public final BooleanPath active = createBoolean("active");

    public final StringPath address = createString("address");

    public final StringPath clientAlias = createString("clientAlias");

    public final StringPath company = createString("company");

    public final StringPath contractDate = createString("contractDate");

    public final NumberPath<Long> idClient = createNumber("idClient", Long.class);

    public final StringPath linkSite = createString("linkSite");

    public final StringPath logo = createString("logo");

    public final StringPath phone = createString("phone");

    public QClient(String variable) {
        super(Client.class, forVariable(variable));
    }

    public QClient(Path<? extends Client> path) {
        super(path.getType(), path.getMetadata());
    }

    public QClient(PathMetadata metadata) {
        super(Client.class, metadata);
    }

}

