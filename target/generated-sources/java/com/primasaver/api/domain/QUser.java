package com.primasaver.api.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QUser is a Querydsl query type for User
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUser extends EntityPathBase<User> {

    private static final long serialVersionUID = -330937190L;

    public static final QUser user = new QUser("user");

    public final StringPath activationCode = createString("activationCode");

    public final NumberPath<Integer> active = createNumber("active", Integer.class);

    public final StringPath company = createString("company");

    public final NumberPath<Integer> createdOn = createNumber("createdOn", Integer.class);

    public final StringPath email = createString("email");

    public final StringPath firstName = createString("firstName");

    public final StringPath getAuthority = createString("getAuthority");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath ipAddress = createString("ipAddress");

    public final BooleanPath isEnabled = createBoolean("isEnabled");

    public final NumberPath<Integer> lastLogin = createNumber("lastLogin", Integer.class);

    public final StringPath lastName = createString("lastName");

    public final DatePath<java.sql.Date> lastPasswordResetDate = createDate("lastPasswordResetDate", java.sql.Date.class);

    public final StringPath passdev = createString("passdev");

    public final StringPath password = createString("password");

    public final StringPath phone = createString("phone");

    public final StringPath rememberCode = createString("rememberCode");

    public final StringPath salt = createString("salt");

    public final NumberPath<Integer> userLevel = createNumber("userLevel", Integer.class);

    public final StringPath username = createString("username");

    public QUser(String variable) {
        super(User.class, forVariable(variable));
    }

    public QUser(Path<? extends User> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUser(PathMetadata metadata) {
        super(User.class, metadata);
    }

}

