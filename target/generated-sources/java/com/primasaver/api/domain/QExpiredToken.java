package com.primasaver.api.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QExpiredToken is a Querydsl query type for ExpiredToken
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QExpiredToken extends EntityPathBase<ExpiredToken> {

    private static final long serialVersionUID = 1326838691L;

    public static final QExpiredToken expiredToken = new QExpiredToken("expiredToken");

    public final DateTimePath<java.time.LocalDateTime> timestamp = createDateTime("timestamp", java.time.LocalDateTime.class);

    public final StringPath token = createString("token");

    public QExpiredToken(String variable) {
        super(ExpiredToken.class, forVariable(variable));
    }

    public QExpiredToken(Path<? extends ExpiredToken> path) {
        super(path.getType(), path.getMetadata());
    }

    public QExpiredToken(PathMetadata metadata) {
        super(ExpiredToken.class, metadata);
    }

}

