package com.primasaver.api.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPanel is a Querydsl query type for Panel
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPanel extends EntityPathBase<Panel> {

    private static final long serialVersionUID = -1674263787L;

    public static final QPanel panel = new QPanel("panel");

    public final NumberPath<Long> capacity = createNumber("capacity", Long.class);

    public final NumberPath<Long> capacityTolerance = createNumber("capacityTolerance", Long.class);

    public final NumberPath<Long> clientId = createNumber("clientId", Long.class);

    public final NumberPath<Long> idPanel = createNumber("idPanel", Long.class);

    public final DateTimePath<java.time.LocalDateTime> installedDate = createDateTime("installedDate", java.time.LocalDateTime.class);

    public final StringPath panelDesc = createString("panelDesc");

    public final StringPath panelName = createString("panelName");

    public final StringPath serialNumber = createString("serialNumber");

    public final DateTimePath<java.time.LocalDateTime> startReport = createDateTime("startReport", java.time.LocalDateTime.class);

    public final NumberPath<Long> zonaId = createNumber("zonaId", Long.class);

    public QPanel(String variable) {
        super(Panel.class, forVariable(variable));
    }

    public QPanel(Path<? extends Panel> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPanel(PathMetadata metadata) {
        super(Panel.class, metadata);
    }

}

