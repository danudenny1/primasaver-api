package com.primasaver.api.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QUserRegister is a Querydsl query type for UserRegister
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUserRegister extends EntityPathBase<UserRegister> {

    private static final long serialVersionUID = -1558469283L;

    public static final QUserRegister userRegister = new QUserRegister("userRegister");

    public final NumberPath<Integer> active = createNumber("active", Integer.class);

    public final StringPath company = createString("company");

    public final NumberPath<Integer> createdOn = createNumber("createdOn", Integer.class);

    public final StringPath email = createString("email");

    public final StringPath firstName = createString("firstName");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> lastLogin = createNumber("lastLogin", Integer.class);

    public final StringPath lastName = createString("lastName");

    public final StringPath passdev = createString("passdev");

    public final StringPath password = createString("password");

    public final StringPath phone = createString("phone");

    public final NumberPath<Integer> userLevel = createNumber("userLevel", Integer.class);

    public final StringPath username = createString("username");

    public QUserRegister(String variable) {
        super(UserRegister.class, forVariable(variable));
    }

    public QUserRegister(Path<? extends UserRegister> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUserRegister(PathMetadata metadata) {
        super(UserRegister.class, metadata);
    }

}

