package com.primasaver.api.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QUsersGroups is a Querydsl query type for UsersGroups
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUsersGroups extends EntityPathBase<UsersGroups> {

    private static final long serialVersionUID = 1926272589L;

    public static final QUsersGroups usersGroups = new QUsersGroups("usersGroups");

    public final NumberPath<Long> groupId = createNumber("groupId", Long.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> userId = createNumber("userId", Long.class);

    public QUsersGroups(String variable) {
        super(UsersGroups.class, forVariable(variable));
    }

    public QUsersGroups(Path<? extends UsersGroups> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUsersGroups(PathMetadata metadata) {
        super(UsersGroups.class, metadata);
    }

}

